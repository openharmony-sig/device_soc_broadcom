# Broadcom

#### Introduction

The raspberry pi 3B uses BCM2835 as the SOC, and the relevant file directory is in device_board_soc_broadcom/bcm2835. At present, it can be used with the SOC (BCM2711) of raspberry pi 4B.

The hardware file contains display and GPU related files.
1.  device_board_soc_broadcom/hardware/display
2.  device_board_soc_broadcom/hardware/gpu



#### Related repos

- [device/board/iscas](https://gitee.com/openharmony-sig/device_board_iscas)
- [vendor/iscas](https://gitee.com/openharmony-sig/vendor_iscas)